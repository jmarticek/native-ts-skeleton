import { MaterialIcons } from '@expo/vector-icons'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import * as React from 'react'

import Colors from '../constants/Colors'
import { BottomTabParamList, LoginParamList, MyAccountParamList, NotificationsParamList } from '../types'
import LanguageSwitch from '../components/LanguageSwitch'
import LoginScreen from '../screens/LoginScreen'
import WebViewScreen from '../screens/WebViewScreen'


const LoginStack = createStackNavigator<LoginParamList>()
const LoginNavigator = () => {
  return (
    <LoginStack.Navigator>
      <LoginStack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{
          headerTitle: 'Přihlaste se',
          headerRight: () => (<LanguageSwitch />),
        }}
      />
      <LoginStack.Screen
        name="WelcomeScreen"
        component={WebViewScreen}
        options={{
          headerTitle: 'Vitejte',
          headerRight: () => (<LanguageSwitch />),
        }}
      />
    </LoginStack.Navigator>
  )
}

const MyAccountStack = createStackNavigator<MyAccountParamList>()
const MyAccountNavigator = () => {
  return (
    <MyAccountStack.Navigator>
      <MyAccountStack.Screen
        name="MyAccountScreen"
        component={WebViewScreen}
        options={{
          headerTitle: 'My account tab',
          headerRight: () => (<LanguageSwitch />),
        }}
      />
    </MyAccountStack.Navigator>
  )
}

const NotificationsStack = createStackNavigator<NotificationsParamList>()
const NotificationsNavigator = () => {
  return (
    <NotificationsStack.Navigator>
      <NotificationsStack.Screen
        name="NotificationsScreen"
        component={WebViewScreen}
        options={{
          headerTitle: 'Notifications tab',
          headerRight: () => (<LanguageSwitch />),
        }}
      />
    </NotificationsStack.Navigator>
  )
}

const BottomTab = createBottomTabNavigator<BottomTabParamList>()
const TabBarIcon = (props: { name: string; color: string }) => {
  return <MaterialIcons size={30} style={{ marginBottom: -3 }} {...props} />
}

const BottomTabNavigator = () => {
  return (
    <BottomTab.Navigator
      initialRouteName="Login"
      tabBarOptions={{ activeTintColor: Colors.tint }}
    >
      <BottomTab.Screen
        name="Login"
        component={LoginNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="MyAccount"
        component={MyAccountNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="person" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Notifications"
        component={NotificationsNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="notifications" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  )
}

export default BottomTabNavigator
