import * as Linking from 'expo-linking'

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          Login: {
            screens: {
              LoginScreen: 'login',
              WelcomeScreen: 'welcome',
            },
          },
          MyAccount: {
            screens: {
              MyAccountScreen: 'myAccount',
            },
          },
          Notifications: {
            screens: {
              NotificationsScreen: 'notifications',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
}
