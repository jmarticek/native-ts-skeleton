import * as React from 'react'
import { StyleSheet } from 'react-native'
import {WebView} from 'react-native-webview'

type INavState = {
  url?: string
  title?: string
  loading?: boolean
  canGoBack?: boolean
  canGoForward?: boolean
}

const WebViewScreen = () => {
  const webview = React.useRef(null)

  const handleNavigation = (newNavState: INavState) => {
  }

  return (
    <WebView
      ref={webview}
      source={{ uri: 'https://google.com/' }}
      onNavigationStateChange={handleNavigation}
    />
  )
}

export default WebViewScreen
