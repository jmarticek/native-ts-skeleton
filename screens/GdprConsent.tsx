import * as React from 'react'
import { StyleSheet, ScrollView } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { Text, View, Button, Input } from '../components/Themed'
import {StackScreenProps} from '@react-navigation/stack'


const GdprConsent = ({navigation}: StackScreenProps<any>) => {
    return (
    <View style={styles.container}>
        <Text h1>Souhlas se zpracovaním osobních údaju</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
})


export default GdprConsent
