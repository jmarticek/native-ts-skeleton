import * as React from 'react'
import { StyleSheet, ScrollView } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { Text, View, Button, Input } from '../components/Themed'
import {StackScreenProps} from '@react-navigation/stack'


const LoginScreen = ({navigation}: StackScreenProps<any>) => {
  const [passwordVisibility, setPasswordVisibility] = React.useState(['visibility', true])
  const [textShown, setTextShown] = React.useState(false)

  const Icon = (props: { name: string, onPress: () => void }) => {
    return <MaterialIcons size={24} color='black' {...props} />
  }

  return (
    <View style={styles.container}>
      <Input
        leftIcon={{ type: 'material', name: 'person' }}
        label="UŽIVATEL (ID)"
      />
      <Input
        leftIcon={
          <Icon
            name={String(passwordVisibility[0])}
            onPress={() => setPasswordVisibility(
              (visibility) => visibility[1] ? ['visibility-off', false] : ['visibility', true]
            )}
          />
        }
        label="HESLO"
        secureTextEntry={Boolean(passwordVisibility[1])}
      />
      <Button
        title="VSTOUPIT"
        titleStyle={{ fontWeight: "bold", padding: 10 }}
        containerStyle={{ padding: 5 }}
        onPress={() => navigation.navigate('WelcomeScreen')}
      />
      <Text h4 onPress={() => setTextShown(!textShown)}>
        Jste tu poprvé?
      </Text>

      {
        textShown &&
        <ScrollView
          style={styles.scrollViewStyle}
          contentContainerStyle={styles.scrollViewContentContainer}
        >
          <Text style={{ textAlign: "justify" }}>
            Každý zaměstnanec má přístup do portálu vytvořen automaticky.
            Není třeba se kamkoliv registrovat, přihlasíte se jednoduše podle tohto pravidla:
                    </Text>
          <Text h4>
            Uživatel (ID)
                    </Text>
          <Text>
            Vaše ID
                    </Text>
          <Text h4>
            Heslo
                    </Text>
          <Text style={{ textAlign: "justify" }}>
            První tři písmena z Vašeho příjmení včetně diakritiky (první písmeno je velké).
            Dále poslední trojčíslí z Vašeho rodného čísla (za lomítkem) a den z data Vašeho narození
            (v případě jednomístného čísla před něj přidejte i nulu)

            Příklad: Novák Jan 831105/1234 = Nov23405
                    </Text>
        </ScrollView>
      }
      <Text onPress={() => navigation.navigate('passwordReset')}>Zapomněli jste heslo?</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  scrollViewStyle: {
    height: 600,
    margin: 20,
    backgroundColor: "#eee"
  },
  scrollViewContentContainer: {
    paddingVertical: 20,
    paddingHorizontal: 5,
  },
})


export default LoginScreen
