const useColorScheme = () => {
  return 'light' as NonNullable<'light' | 'dark'>
}

export default useColorScheme