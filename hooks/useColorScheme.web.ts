// useColorScheme from react-native does not support web currently. 
const useColorScheme = () => {
  return 'light'
}

export default useColorScheme