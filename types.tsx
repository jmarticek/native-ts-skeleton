export type RootStackParamList = {
  Root: undefined
  NotFound: undefined
}

export type BottomTabParamList = {
  Login: undefined
  MyAccount: undefined
  Notifications: undefined
}

export type LoginParamList = {
  LoginScreen: undefined
  WelcomeScreen: undefined
}

export type MyAccountParamList = {
  MyAccountScreen: undefined
}

export type NotificationsParamList = {
  NotificationsScreen: undefined
}