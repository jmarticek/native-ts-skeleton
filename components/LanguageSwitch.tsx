import * as React from 'react'

import { Button } from 'react-native'


type ILanguageSwitch = {
    title?: string
    color?: string
    onPress?: () => void
}

const LanguageSwitch = (props: ILanguageSwitch) => {
  return <Button title="CZ  |  EN" onPress={() => console.log('langSwitch')} {...props} />
}

export default LanguageSwitch