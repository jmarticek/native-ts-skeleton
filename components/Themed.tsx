import * as React from 'react'
import { View as DefaultView, StyleSheet } from 'react-native'
import { Input as DefaultInput, Button as DefaultButton, Text as DefaultText } from 'react-native-elements'
import Colors from '../constants/Colors'
import useColorScheme from '../hooks/useColorScheme'

export const useThemeColor = (
  props: { light?: string; dark?: string },
  colorName: keyof typeof Colors
) => {
  const theme = useColorScheme()
  const colorFromProps = props[theme]

  if (colorFromProps) {
    return colorFromProps
  } else {
    return Colors[colorName]
  }
}

type ThemeProps = {
  lightColor?: string
  darkColor?: string
}

export type TextProps = ThemeProps & DefaultText['props']
export type ViewProps = ThemeProps & DefaultView['props']
export type ButtonProps = ThemeProps & DefaultButton['props']
export type InputProps = ThemeProps & DefaultInput['props']


export const Text = (props: TextProps) => {
  const { style, lightColor, darkColor, ...otherProps } = props
  const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text')

  return <DefaultText style={[{ color }, style]} {...otherProps} />
}

export const View = (props: ViewProps) => {
  const { style, lightColor, darkColor, ...otherProps } = props
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'background')

  return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />
}

export const Button = (props: ButtonProps) => {
  return (
    <DefaultButton
      buttonStyle={styles.buttonStyle}
      containerStyle={styles.buttonContainerStyle}
      {...props}
    />
  )
}

export const Input = (props: InputProps) => {
  return (
  <DefaultInput 
    inputContainerStyle={styles.inputContainerStyle}
    {...props}
  />
  )
}


const styles = StyleSheet.create({
  inputContainerStyle: {
    borderStyle: "solid",
    borderWidth: 4,
    borderBottomWidth: 4,
    borderRadius: 10,
    padding: 10,
  },
  buttonStyle: {
  },
  buttonContainerStyle: {
  },
})
